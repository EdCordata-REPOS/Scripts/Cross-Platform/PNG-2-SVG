# PNG 2 SVG
Simple script, written in
[Ruby](https://www.ruby-lang.org/en/),
to convert PNG to SVG.

Not optimized at all - will convert pixel by pixel.


<br/>


## Usage
```bash
ruby png_to_svg.rb input-file-name.png
```
This will generate `output.svg`


<br/>


## License
[CC BY-SA 4.0 DEED](https://creativecommons.org/licenses/by-sa/4.0/)

