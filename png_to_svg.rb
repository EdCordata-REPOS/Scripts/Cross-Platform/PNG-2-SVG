# validate file path
# ------------------------------------------------------------------------------
raise 'USAGE: ruby png_to_svg.rb file.png' if ::ARGV.empty?
raise 'File must exist' unless File.exist?(::ARGV.first)
# ------------------------------------------------------------------------------



# validate required gems
# ------------------------------------------------------------------------------
begin
  require 'chunky_png'
rescue Exception => ex
  raise "'chunky_png' is required. Run 'gem install chunky_png'"
end
# ------------------------------------------------------------------------------


# load image
# ------------------------------------------------------------------------------
image = ::ChunkyPNG::Image.from_file(::ARGV.first)
# ------------------------------------------------------------------------------


# read image data
# ------------------------------------------------------------------------------
data   = []
colors = []

(0...image.height).each do |y|
  row = []

  (0...image.width).each do |x|
    pixel = image[x, y]

    color = [
      ChunkyPNG::Color.r(pixel),
      ChunkyPNG::Color.g(pixel),
      ChunkyPNG::Color.b(pixel),
      ChunkyPNG::Color.a(pixel)
    ].join(',')

    colors << color unless colors.include?(color)

    row << colors.index(color)
  end

  data << row
end
# ------------------------------------------------------------------------------


# build svg
# ------------------------------------------------------------------------------
SVG_STR = [
  '<svg xmlns="http://www.w3.org/2000/svg"',
  '  x="0"',
  '  y="0"',
  '  width="{WIDTH}"',
  '  height="{HEIGHT}"',
  '  viewBox="0 0 {WIDTH} {HEIGHT}"',
  '  shape-rendering="crispEdges"',
  '  preserveAspectRatio="xMidYMid meet">',
  '',
  '  <style>',
  '    {STYLES}',
  '  </style>',
  '',
  '  <g>',
  '    {CONTENT}',
  '  </g>',
  '',
  '</svg>'
]

SVG_PIXEL = '<rect x="{X}" y="{Y}" width="1" height="1" class="c-{C}"/>'

svg_str = SVG_STR.join("\n")
svg_str = svg_str.gsub('{WIDTH}',  data[0].count.to_s)
svg_str = svg_str.gsub('{HEIGHT}', data.count.to_s)

svg_items = []

data.each_with_index do |row, row_index|
  data[row_index].each_with_index do |column, column_index|
    svg_item = SVG_PIXEL
    svg_item = svg_item.gsub('{Y}', row_index.to_s)
    svg_item = svg_item.gsub('{X}', column_index.to_s)
    svg_item = svg_item.gsub('{C}', column.to_s)
    svg_items << svg_item
  end
end

svg_str = svg_str.gsub('{CONTENT}', svg_items.join("\n    "))

svg_styles = []

colors.each_with_index do |color, color_index|
  r, g, b, a = color.split(',')
  rgb        = [r, g, b].join(',')
  opacity    = ( a == '0' ) ? (0) : ( (a.to_f / 255.0 * 100.0) / 100.0 )

  if color == '0,0,0,0'
    svg_styles << ".c-#{color_index} { fill: transparent; }"
  else
    svg_styles << ".c-#{color_index} { fill: rgb(#{rgb}); opacity: #{opacity}; }"
  end
end

svg_str = svg_str.gsub('{STYLES}', svg_styles.join("\n    "))

# ------------------------------------------------------------------------------


# Save SVG
# ------------------------------------------------------------------------------
File.write('output.svg', svg_str)
# ------------------------------------------------------------------------------

